import Component from './component';
import AudioPlayer from './media/audio-player';

export default class Gallery extends Component {
  constructor($element) {
    super($element);

    // select all right gallery items
    const $selectableItemss = this.target.querySelectorAll('.selected-item');

    // set events for all found items
    $selectableItemss.forEach(($item) => Gallery.setEvents($item));
  }

  static setEvents($item) {
    const selectedId = $item.id;
    const $placeholder = $item.querySelector('.selected-item__placeholder');

    document.addEventListener('selection.change', (e) => {
      if (selectedId === e.detail.id) {
        const data = e.detail;
        const idTail = '-duplicated';

        switch (data.task) {
          // singlechoice
          case 'change':
            Gallery.removeItemFromSelect($item);
            Gallery.addItemFromSelect($item, idTail, data);
            break;

          // multichoice
          case 'select':
            // hide placeholder
            $placeholder.classList.add('is-hidden');

            Gallery.addItemFromSelect($item, idTail, data);
            break;

          case 'unselect':
            Gallery.removeItemFromSelect($item, data.itemId + idTail);

            // show placeholder if its have to
            if ($item.childElementCount === 1) {
              $placeholder.classList.remove('is-hidden');
            }
            break;

          default:
            console.error('ERROR: Undefinated task type.');
        }
      }
    });
  }

  static addItemFromSelect($item, idTail, data) {
    const $dupNode = data.$node.cloneNode(true);
    $dupNode.setAttribute('data-id', data.itemId + idTail);

    // add listeners to audio
    if ($dupNode.hasAttribute('data-player-audio')) {
      AudioPlayer.addEventListeners($dupNode, () => {});
    }

    $item.appendChild($dupNode);
  }

  static removeItemFromSelect($item, dataId = undefined) {
    if (dataId) {
      const $dupNode = $item.querySelector(`[data-id=${dataId}]`);
      $dupNode.remove();
    }
    else {
      $item.firstChild.remove();
    }
  }
}
